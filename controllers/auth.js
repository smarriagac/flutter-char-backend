const { response } = require('express');
const bcrypt = require('bcryptjs');

const Usuario = require('../models/usuario');
const { generarJWT } = require('../helpers/jwt');

const crearUsuario = async (req, res = response ) => {

    const { email, password } = req.body;

    try {

        const existeEmail = await Usuario.findOne({email});
        if(existeEmail){
            return res.status(400).json({
                ok: false,
                msg: 'El correo ya esta registrado'
            })
        }

        const usuario = new Usuario(req.body);

        // Encriptar contrasenha
        const salt = bcrypt.genSaltSync();
        usuario.password = bcrypt.hashSync(password, salt);

        // grabar en base de datos
        await usuario.save();

        // generar mi JWT (jason web token)
        const token = await generarJWT( usuario.id );

        // enviar informacion al usuario
        res.json({
            ok: true,
            Usuario: usuario,
            token,
        });
        
    } catch (error) {
        console.log(error);
        res.status(500).json({
            ok: false,
            msg: 'Hable con el administrador'
        })
    }
}

    // LOGIN

    const login = async (req, res = response ) => {

        const {email, password} = req.body;

        try {
            
            const usuarioDb = await Usuario.findOne({email});
            if (!usuarioDb) {
                return res.status(404).json({
                    ok: false,
                    msg: 'Email no encontrado'
                });
            }

            // validar el password
            const validPassword = bcrypt.compareSync(password, usuarioDb.password);
            if(!validPassword) {
                return res.status(400).json({
                    ok: false,
                    msg: 'La contrasenha no es valida'
                });
            }

            // Generar el JWT
            const token = await generarJWT(usuarioDb.id);

            res.json({
                ok: true,
                Usuario: usuarioDb,
                token
            });


        } catch (error) {
            console.log(error);
            return res.status(500).json({
                ok: false,
                msg: 'Hable con el administrador'
            })
        }
    }

    const renewToken = async (req, res = response) => {

        
        const  uid  = req.uid;
        
        const token = await generarJWT(uid);
    
        const usuario = await Usuario.findById(uid);

        res.json({
            ok: true,
            Usuario: usuario,
            token
        });

    }

module.exports = {
    crearUsuario,
    login,
    renewToken
}